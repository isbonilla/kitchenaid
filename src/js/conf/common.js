module.exports = {
	"dumbo": {
		"operationCode": ""
	},
	"modules": {
		"adotmob": false,
		"hashMemberId": false
	},
	"mixpanel": {
		"preprod": {
			"token": "eae7ed6b6420b0befde82c2e721ae890",
			"apiHost": "https://data.services.preprod.vente-privee.com/frontservices/api-tracking"
		},
		"prod": {
			"token": {
				"veepee": {
					"fr": "a3cb8bb25549b48e7409f1211fc04976",
					"es": "2cf592140dba3dc4402b360233a6dd45",
					"de": "9b6b75a37c6eff06bf14132f67d55e37",
					"it": "b1d06f62ef92319ca25f11d487b35d8b",
					"at": "e1fcf2661096212f64500ac5c70b6956",
					"befr": "69a6e42ce2809abc255bb9ae23faeb4b",
					"benl": "974bdc6ae4584aa38454bab8fa711507",
					"lu": "29c32372870a21fdfd1d5734f4fafabb",
					"nl": "adc328f10cd88ca5604020c26154816b"
				},
				"privalia": {
					"es": "e39a3b46dfc09817178d1c18a2bceda5",
					"it": "1cc66c7dc984f3f7830675b6c8dd0e1f"
				}
			},
			"apiHost": "https://data.services.vente-privee.com/frontservices/api-tracking"
		}
	},
	"onetrust": {
		"veepee": {
			"fr": "96033a43-b4a6-42a7-acd0-c1fa033e95cc",
			"es": "cf80f426-9d4a-41a8-802d-84998898244f",
			"de": "e97c38a9-995f-4113-8856-4038ae6d8152",
			"it": "7d692c2d-6049-4fa5-a6a8-125ab211dc86",
			"at": "ad42a790-506e-452f-b968-7e25ec6be129",
			"befr": "05fe2809-b98e-4505-a7a3-1bed66436d89",
			"benl": "05fe2809-b98e-4505-a7a3-1bed66436d89",
			"lu": "0d4717d0-337e-40e7-a622-e5e2bfc71575",
			"nl": "a9247576-4d77-46aa-bb09-89a70a9b070c"
		},
		"privalia": {
			"es": "b4d197e7-a7be-4e0b-af9e-4f72a6becacd",
			"it": "929bc1ff-01d6-4991-8ad7-24dd6fd26532"
		}
	},
	"sites": {
		"veepee": {
			"fr": 1,
			"de": 2,
			"es": 3,
			"it": 4,
			"at": 7,
			"befr": 70,
			"benl": 71,
			"lu": 73,
			"nl": 76
		},
		"privalia": {
			"es": 66,
			"it": 69
		}
	},
	"redirectionTypes": {
		"1": "Website of the brand",
		"2": "Brandsplace",
		"3": "Flashsales",
		"4": "Home"
	},
	"api": {
		"preprod": {
			"instantWin": "https://brand-mail-api-preprod.front.vpgrp.io/wintime/getresultwintime",
			"baseURL": "https://brand-mail-api-preprod.front.vpgrp.io/",
			"hashMemberId": "https://brand-mail-api-preprod.front.vpgrp.io/opespe/pb_clarins78",
			"mail": "http://brand-mail-api-preprod.front.vpgrp.io/projects/PB_CLARINS78/users"
		},
		"prod": {
			"instantWin": "https://brand-mail-api.front.vpgrp.net/wintime/getresultwintime",
			"baseURL": "https://brand-mail-api.front.vpgrp.net/",
			"hashMemberId": "https://brand-mail-api.front.vpgrp.net/opespe/pb_clarins78",
			"mail": "https://brand-mail-api.front.vpgrp.net/projects/PB_CLARINS78/users"
		}
	},
	"operation": {
		"brandName": "CLARINS",
		"siteType": "Carte Rose",
		"redirectionType": "",
		"id": "00000",
		"universeID": "0000",
		"partnerID": "18739",
		"date": "December 31, 2022 07:00:00"
	}
};
