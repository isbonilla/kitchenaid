import cssVars from 'css-vars-ponyfill'
import Vue from 'vue'
import VueYouTubeEmbed from 'vue-youtube-embed'
import VueParallaxJs from 'vue-parallax-js'
import VueScrollReveal from 'vue-scroll-reveal'

// App
import App from '@/App.vue'

// Global components
import Frow from '@/blocks/Frow/Frow.vue'
import FrowCol from '@/blocks/FrowCol/FrowCol.vue'
import Img from '@/components/Img/Img.vue'
import Click from '@/components/Click/Click.vue'
import Fonts from '@/components/_mods/Fonts/Fonts.vue'
import DynamicTrad from '@/components/DynamicTrad/DynamicTrad.vue'

// Plugins
import hotjar from '@/js/plugins/hotjar'
import i18n from '@/js/plugins/i18n'
import '@/js/plugins/links'
import setupApi from '@/js/plugins/setupApi'
import tracking from '@/js/plugins/tracking'
import '@/js/plugins/scrollManager'
import VueScrollTo from 'vue-scrollto'
import vhcheck from '@/js/plugins/vhcheck'
import { InlineSvgPlugin } from 'vue-inline-svg'
import { EventBus } from '@/js/plugins/event-bus'

// Conf
import '@/js/conf/'

// Directives
import { loader, timer, timestamp, display, parallax, livestream } from '@/js/directives'

// Store
import store from '@/store/'

// GSAP
import gsap from 'gsap'

// FULLPAGE
import VueFullPage from 'vue-fullpage.js'

// APPEAR ANIMATION
import ScrollAnimationMixin from '@/js/mixins/scrollAnimations/apparitions/apparitions.js'

import { ScrollTrigger } from 'gsap/ScrollTrigger'
gsap.registerPlugin(ScrollTrigger)
ScrollTrigger.config({
    // Do not remove. On mobile devices, prevent 'jump' from bar address resize on scroll animations. You are welcome :)
    // default is "resize,visibilitychange,DOMContentLoaded,load" so you can remove "resize" from the list:
    autoRefreshEvents: 'visibilitychange,DOMContentLoaded,load'
})

Vue.config.productionTip = false
Vue.use(VueYouTubeEmbed, { global: true, componentId: 'youtube-media' })
Vue.use(VueFullPage)
Vue.use(VueParallaxJs)
Vue.use(VueScrollTo)
Vue.use(InlineSvgPlugin)
Vue.use(VueScrollReveal)

// Components
Vue.component('Frow', Frow)
Vue.component('FrowCol', FrowCol)
Vue.component('Img', Img)
Vue.component('Click', Click)
Vue.component('Fonts', Fonts)
Vue.component('DynamicTrad', DynamicTrad)

// Directives
Vue.directive('loader', loader)
Vue.directive('timer', timer)
Vue.directive('timestamp', timestamp)
Vue.directive('display', display)
Vue.directive('parallax', parallax)
Vue.directive('livestream', livestream)

// Mixins
Vue.mixin(ScrollAnimationMixin)

cssVars({
    rootElement: document // default
})

// Force to start at top of the page
window.onbeforeunload = () => {
    window.scrollTo(0, 0)
}

new Vue({
    i18n,
    hotjar,
    store,
    render: h => h(App),
    beforeMount() {
        tracking()
        setupApi(this.$config.common.dumbo.operationCode)

        // Used to create a global var (exemple of use : this.$config or {{ $config }} in a template)
        Vue.prototype.$gsap = gsap
        Vue.prototype.$ScrollTrigger = ScrollTrigger
        Vue.prototype.$EventBus = EventBus
        vhcheck()
        this.$store.dispatch('member/setMemberId')
        this.$store.dispatch('member/hashMemberId')
        this.$store.dispatch('debug/setDebug') // init debug mode if #debug in url
        this.$store.dispatch('general/updateCurrentDate')

        if (this.$route) { // If vue-router used then that's not a One Page
            this.$store.dispatch('general/updateCurrentPageName', this.$route.name)
            this.$store.dispatch('general/updatePreviousPageName', this.$route.name)
        }
    },
    watch: {
        $route(to, from) {
            setTimeout((_) => {
                this.$store.dispatch('general/updatePreviousPageName', from.name)
                this.$store.dispatch('general/updateCurrentPageName', to.name)
                this.$tracking.viewTab()
            }, 0)
        }
    }
}).$mount('#app')
