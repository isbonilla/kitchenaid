export const benl = {
    consent: {
        txt: [
            'Door deze inhoud te openen ',
            '<b>verklaar je dat je minstens 18 jaar oud bent.</b>'
        ],
        yes: 'Ik ben ouder dan 18 jaar',
        no: 'Ik ben jonger dan 18 jaar'
    }
}
