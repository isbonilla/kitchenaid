'use strict'

import { getQueryString } from '@/js/tools'
import { sites } from '@/js/conf/common'

// Default to veepee.fr
let locale = getQueryString('lang') || 'fr'
let domain = getQueryString('lang') || 'fr'
let site = getQueryString('site') || 'veepee'
let language = 'fr'
let languageCode = `${domain}-${domain}`
let APIbaseURL = 'https://'

let hostname = window.location.hostname

// simulate hostname
if (hostname === 'localhost' || window.location.hostname.indexOf('mutagency') !== -1) {
    if (site === 'privalia') { // If Privalia
        hostname = `${domain}.${site}.com`
    } else if (locale.includes('be')) { // If Belgique
        const lang = locale.replace('be', '')
        hostname = `${lang}.${site}.be`
    } else { // If Veepee classic
        hostname = `www.${site}.${locale}`
    }
}

// Check hostname (for preprod/prod)
if (hostname.indexOf('privalia') > -1) {
    // xx.privalia.com
    domain = hostname.split('.').shift()
    if (domain.indexOf('preprod') !== -1) domain = domain.split('preprod-').pop()
    locale = `${domain}pr`
    site = 'privalia'
    languageCode = `${domain}-${domain}`
    language = domain
    APIbaseURL += `${domain}.${site}.com`
} else if (hostname.indexOf('veepee') > -1) {
    // www.veepee.xx
    domain = hostname.split('.').pop()
    if (domain === 'be') {
        let lang = hostname.split('.').shift()
        if (lang.indexOf('preprod') !== -1) lang = lang.split('preprod-').pop()
        APIbaseURL += `${lang}.${site}.${domain}`
        locale = `be${lang}`
        domain = locale
        languageCode = `${lang}-be`
        language = lang
    } else {
        locale = domain
        languageCode = `${locale}-${locale}`
        language = locale
        APIbaseURL += `www.${site}.${domain}`
    }
} else {
    // Hosted on Demo servers
    APIbaseURL += 'www.veepee.fr'
}

// Site ID
const siteId = sites[site][domain]

// Check if "Carte Rose" or "Pink Brand" for footer/loader
const isCarteRose = locale === 'befr' || locale === 'lu' || locale === 'fr' || locale === 'benl'
const isPrivalia = site === 'privalia'
const isVex = locale === 'befr' || locale === 'benl' || locale === 'nl' || locale === 'lu'

// Set favicon for Privalia
if (site === 'privalia') {
    document.querySelector('#favicon').setAttribute('href', 'favicon-privalia.png')
}

export default {
    hostname,
    locale,
    domain,
    site,
    isCarteRose,
    isPrivalia,
    isVex,
    languageCode,
    siteId,
    APIbaseURL,
    language
}
