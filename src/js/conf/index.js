'use strict'
import common from './common'
import lang from './lang'
import detectEnv from './detectEnv'
import linkHome from './linkHome'
import Vue from 'vue'

let config = {
    common: common,
    lang: lang,
    detectEnv: detectEnv,
    linkHome: linkHome
}

Vue.prototype.$config = config

export default config
