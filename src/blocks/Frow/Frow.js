'use strict'

/**
 * Creates a row element that is required for the grid system.<br />
 * Can contain multiple \<FrowCol>\</FrowCol> components.<br /><br />
 * Frow is globally loaded, so you don't need to import it when you create a new block.<br />
 * You can use pre-defined classes on Frow like "center", "gutters", etc. See the Frow section in the wiki for more details.
 */
export default {
    name: 'Frow',
    props: {
        /**
         * Set the row's background color.
         */
        backgroundColor: {
            type: String
        }
    }
}
