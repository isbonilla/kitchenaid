'use strict'

export default [
    {
        slug: '', // Veepee and Privalia homepage
        id: ''
    },
    {
        slug: 'fashion',
        id: 4
    },
    {
        slug: 'home',
        id: 8
    },
    {
        slug: 'beauty',
        id: 26
    },
    {
        slug: 'travel',
        id: 64
    },
    {
        slug: 'brandsplace',
        id: 82
    },
    {
        slug: 'kids',
        id: 20
    },
    {
        slug: 'sport',
        id: 22
    },
    {
        slug: 'kitchen-dining-wine',
        id: 512
    },
    {
        slug: 'smartshopping',
        id: 21
    },
    {
        slug: 'showtime',
        id: 128
    },
    {
        slug: 'express',
        id: 262144
    },
    {
        slug: 'summercamp',
        id: 158
    }
]
