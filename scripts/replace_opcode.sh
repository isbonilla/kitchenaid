#!/usr/bin/env bash

# Usage :
# - Go to the root of the repository
# - run : ./script/replace_opcode.sh op-code-number [auto_update:bool <true>] [remote_branch:string <master>]
#
# All "[opcode]" strings and their variants will be replaced by "op-code-number" in specific files
# (defined in the files* variables below).
#
# Note : this script CANNOT be used to rename or migrate the base-vuejs project itself (you should do this manually),
# but only its descendants (minisites forked from base-vuejs).
#
# Migration flag -m is triggered by scripts like these :
# https://confluence.vptech.eu/pages/viewpage.action?pageId=95816408

GREEN="\033[1;32m"
RED="\033[1;31m"
DEF="\033[0m"

# Check params and flags

migration=false
while getopts :m opt; do
    case $opt in
        m) migration=true;;
    esac
done

shift $(( OPTIND - 1 ))

auto_update=${2:-'true'}
remote_branch=${3:-'master'}

filesWithDash=(
    ./context/minisites/ci/nginx/nginx.conf \
    ./context/minisites/ci/nginx/default \
    ./context/minisites/ci/Dockerfile \
    ./context/minisites/ci/preprod.json \
    ./context/minisites/ci/prod.json \
    ./context/minisites/.gitlab-ci.yml \
    ./public/index.html \
)

filesOpCodeStrictGit=(
    ./context/minisites/ci/preprod.json \
    ./context/minisites/ci/prod.json \
)

# Make sed compatible for both MacOS and Windows
OS=`uname`
function replace_in_file() {
    if [ "$OS" = 'Darwin' ]; then
        # MacOS
        sed -i '' -e "$1" "$2"
    else
        # Linux/Windows
        sed -i'' -e "$1" "$2"
    fi
}

if ! $migration; then
    filesWithUnderscore=(
        ./src/js/conf/common.js \
    )

    filesWithLowerCode=(
        ./src/js/conf/common.js \
    )
fi

tmpJsonFile="./src/js/conf/config.tmp.js"

opcode_template='[opcode]'
opcode_template_escaped='\[opcode\]'
opcode_lower_template='[opcode_lower]'
opcode_lower_template_escaped='\[opcode_lower\]'
opcode_strict_git='[opcode_strict_git]'
opcode_strict_git_escaped='\[opcode_strict_git\]'
opcode_lower_dash=`echo $1 | tr [:upper:] [:lower:] | tr "_" "-" `
opcode_upper_underscore=`echo $1 | tr [:lower:] [:upper:] | tr "-" "_" `
opcode_lower=`echo $1 | tr [:upper:] [:lower:] `

if [[ -z "$opcode_lower_dash" ]]; then
    echo "usage: bash ./script/replace_opcode.sh opcode" 1>&2
    exit 1
fi

# Make git case sensitive on MacOS

git config --global core.ignorecase false

# Reset all configuration files for CI purposes

if $auto_update; then

    git remote add reset git@git.vptech.eu:veepee/offerdiscovery/products/brandmanagement/base-vuejs.git
    git fetch reset

    echo "Pulling all CI files from the remote repository"

    git checkout reset/${remote_branch} -- context/minisites

    for file in "${filesWithDash[@]}"; do
        git checkout reset/${remote_branch} -- $file
    done

    for file in "${filesOpCodeStrictGit[@]}"; do
        git checkout reset/${remote_branch} -- $file
    done

    for file in "${filesWithUnderscore[@]}"; do
        git checkout reset/${remote_branch} -- $file
    done

    git remote remove reset

fi

resetFile() {
node > $1<<EOF
var data = require('$2');
data.dumbo.operationCode = "[opcode]";
console.log("module.exports = " + JSON.stringify(data, null, "\t") + ";");
EOF
}

echo "Replacing all op-code-number instances with string '[opcode]' in:"
for file in "${filesWithUnderscore[@]}"; do
    echo "Reset file : ${tmpJsonFile} ${file}"
    resetFile ${tmpJsonFile} ${file}

    mv ${tmpJsonFile} ${file}
done

echo "Replacing all $opcode_template by $opcode_lower_dash..."
for file in "${filesWithDash[@]}"; do
    test=$(replace_in_file "s/$opcode_template_escaped/$opcode_lower_dash/g" "$file")
    if [[ $? -ne 0 ]]; then
        printf "%-30s$RED%s$DEF\n" $file "Failed to replace" 1>&2
        err=1
    else
        printf "%-30s$GREEN%s$DEF\n" $file "OK"
    fi
done

echo "Replacing all $opcode_template by $opcode_upper_underscore..."
for file in "${filesWithUnderscore[@]}"; do
    test=$(replace_in_file "s/$opcode_template_escaped/$opcode_upper_underscore/g" "$file")
    if [[ $? -ne 0 ]]; then
        printf "%-30s$RED%s$DEF\n" $file "Failed to replace" 1>&2
        err=1
    else
        printf "%-30s$GREEN%s$DEF\n" $file "OK"
    fi
done

echo "Replacing all $opcode_lower_template by $opcode_lower..."
for file in "${filesWithLowerCode[@]}"; do
    test=$(replace_in_file "s/$opcode_lower_template_escaped/$opcode_lower/g" "$file")
    if [[ $? -ne 0 ]]; then
        printf "%-30s$RED%s$DEF\n" $file "Failed to replace" 1>&2
        err=1
    else
        printf "%-30s$GREEN%s$DEF\n" $file "OK"
    fi
done

echo "Replacing all $opcode_strict_git by $opcode_lower..."
for file in "${filesOpCodeStrictGit[@]}"; do
    test=$(replace_in_file "s/$opcode_strict_git_escaped/$opcode_lower/g" "$file")
    if [[ $? -ne 0 ]]; then
        printf "%-30s$RED%s$DEF\n" $file "Failed to replace" 1>&2
        err=1
    else
        printf "%-30s$GREEN%s$DEF\n" $file "OK"
    fi
done

# Move updated files to their final destination (replacement)

rm -rf ./.gitlab-ci.yml
rm -rf ./ci

cp -f ./context/minisites/.gitlab-ci.yml ./.gitlab-ci.yml
cp -rf ./context/minisites/ci ./ci
rm -rf ./context

# Send exit code

if [[ -z "$err" ]]; then
    exit 0
else
    exit 1
fi
