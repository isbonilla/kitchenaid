export const it = {
    consent: {
        txt: [
            'Accedendo a questo contenuto',
            '<b> dichiari di avere almeno 18 anni.</b>'
        ],
        yes: 'Ho più di 18 anni',
        no: 'Ho meno di 18 anni'
    }
}
