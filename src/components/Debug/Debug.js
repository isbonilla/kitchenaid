'use strict'

import { createNamespacedHelpers } from 'vuex'

const { mapState, mapActions } = createNamespacedHelpers('debug')

export default {
    name: 'Debug',
    data: () => ({
        ...mapState([
            'showLabelTracking',
            'showHref'
        ]),
        trackedEl: [],
        trackersPopin: false,
        previewDate: null
    }),
    computed: {
        previewUrl() {
            const date = new Date(this.previewDate).toISOString()
            const url = window.location.href.replace('#debug', `?previewDate=${date}`)

            return url
        }
    },
    mounted() {
        this.trackedEl = document.querySelectorAll('.click__tracked')
    },
    methods: {
        ...mapActions([
            'reversePannelBool',
            'reverseLabelTrackingBool',
            'reverseHrefBool'
        ]),
        openApp() {
            let url = window.location.href.replace('#debug', '')
            window.location.href = `appvp://openwv/${encodeURIComponent(url)}`
        },
        canBeShown(index) {
            return !(this.$store.state.live.embedStyle === 'veepee-sandbox' && this.trackedEl[index].childNodes[0].innerHTML === 'LIVE')
        },
        goToElement(index) {
            document.querySelectorAll('.click')[index].scrollIntoView()
            document.querySelectorAll('.click__tracked')[index].classList.add('animation')
            setTimeout((_) => { document.querySelectorAll('.click__tracked')[index].classList.remove('animation') }, 1200)
            this.trackersPopin = false
        }
    }
}
