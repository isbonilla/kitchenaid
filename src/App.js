import Loader from '@/blocks/Loader/Loader.vue'
import Home from '@/blocks/Home/Home.vue'
import Header from '@/blocks/_modulus/blocks/Header/Header.vue'
import Footer from '@/blocks/Footer/Footer.vue'
import RedirectionPopup from '@/blocks/RedirectionPopup/RedirectionPopup.vue'
import Sticky from '@/components/Sticky/Sticky.vue'
import BackToHome from '@/components/BackToHome/BackToHome.vue'
import Debug from '@/components/Debug/Debug.vue'
import Offer from '@/blocks/_modulus/blocks/Offer/Offer.vue'
// new blocks (do not remove this comment)
import Slider from './blocks/_modulus/blocks/Slider/Slider.vue'
import Cards from './blocks/_modulus/blocks/Cards/Cards.vue'

import { createNamespacedHelpers } from 'vuex'
import { playMasterTimeline } from '@/js/plugins/timelineManager'
const { mapState, mapGetters, mapActions } = createNamespacedHelpers('loader')

export default {
    name: 'app',
    components: {
        Loader,
        Home,
        Header,
        Offer,
        Sticky,
        Footer,
        RedirectionPopup,
        BackToHome,
        Debug,
        Slider,
        Cards
    },
    data() {
        return {
            animationIntro: null
        }
    },
    computed: {
        ...mapState([
            'contentInitialized'
        ]),
        ...mapGetters([
            'nbAssets',
            'isLoaded',
            'isConsentLoaded'
        ]),
        getImages: function() {
            let context = require.context('@/assets/img/backgrounds/', false, /\.*$/)
            let images = context.keys().map(context)
            context = this.$config.detectEnv.isDesktop ? require.context('@/assets/img/backgrounds/desk/', false, /\.*$/) : require.context('@/assets/img/backgrounds/mob/', false, /\.*$/)
            images = images.concat(context.keys().map(context))
            return images // return deep url images
        }
    },
    watch: {
        isLoaded(loaded) {
            if (loaded) {
                this.stopLoadingTimer()
                this.isAnimationIntroSet()
                playMasterTimeline()
            }
        }
    },
    beforeMount() {
        this.startLoadingTimer()
    },
    methods: {
        ...mapActions([
            'setLoadingTime'
        ]),
        startLoadingTimer() {
            this.$disableScroll()
            this.startTime = Date.now()

            // if loading since 10 seconds, then send mixpanel error event
            setTimeout((_) => {
                if (!this.isLoaded && this.contentInitialized) {
                    // Immediately send mixpanel hit if the tracking is already initialized, else put the error in the store
                    if (this.$tracking) {
                        this.$tracking.error('loading website', this.nbAssets)
                    } else {
                        this.$store.dispatch('loader/addMixpanelError', {
                            label: 'loading website',
                            message: this.nbAssets
                        })
                    }
                }
            }, 10000)
        },
        stopLoadingTimer() {
            this.endTime = Date.now()
            let loadingTime = this.endTime - this.startTime
            this.setLoadingTime(loadingTime)
            this.isConsentLoaded && this.$enableScroll()
            console.log(`Loading time: ${loadingTime}ms`)
        },
        isAnimationIntroSet() {
            if (this.$options.components.Header) {
                this.$nextTick(() => {
                    const header = this.$children.find(child => child.$options.name === 'Header')
                    if (header && header.animationIntro) {
                        this.$disableScroll()
                    }
                })
            }
        }
    }
}
