export const at = {
    consent: {
        txt: [
            'Durch den Zugriff auf diese Inhalte erklärst Du,',
            '<b> dass Du mindestens 18 Jahre alt bist.</b>'
        ],
        yes: 'Ich bin über 18 Jahre alt',
        no: 'Ich bin unter 18 Jahre alt '
    }
}
