# URL Generator

The URL Generator can be used to get any links that redirects to a website.
That can be the links to redirect on the website (App and Browser) or the links to redirect on products, etc...

You can get those links :
- **Portal** (cf the next section)
- **App** (redirects on the website, in the app)
- **Browser** (redirects on the website, in the browser)
- **Flashsale** (any redirection to a flashsale)
- **Brandsplace** (any redirection to the brandsplace)


*** Usage  ***

You have to be connected to the VPN as it's **only available on preproduction**.

1. Goes to the URL Generator : https://preprod.veepee.fr/experiences/url-generator/
2. Select the Country
3. Select the type of the redirection (Browser for the website links, Flashsale/Brandsplace for the flashsale/brandsplace links)
4. Select the type of the link
5. Fill the information needed for your link in the **Link parameters** section
6. (optional) Check the auto redirect if you want to be automatically redirected on the browser version, on mobile
7. Click on the **Generate Link** button, then you can copy the links generated bellow
