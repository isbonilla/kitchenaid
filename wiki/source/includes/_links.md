# Links

> `sale`

```html
<!-- With default operationId (in common.js) -->
<a :href="$getLink('sale')">J'en profite</a>

<!-- Override operationId -->
<a :href="$getLink('sale', { operationId: 109175 })">J'en profite</a>
```

> `product`

```html
<a :href="$getLink('product', {
    operationId: 109175,
    universeId: 8111012,
    productId: 24479986
})">J'en profite</a>
```

> `filter`

```html
<a :href="$getLink('filter', {
    operationId: 109175,
    universeId: '8111012'
})">J'en profite</a>
```

> `browser`

```html
<a :href="$getLink('browser', {
    href: 'https://brand-media-demo.front.vpgrp.net/experiences/crb-quitoque4/'
})">J'en profite</a>
```

> `home` : check the `src/js/conf/linkHome.js` file for all available homepages

```html
<a :href="$getLink('home', {
    slug: 'travel'
})">J'en profite</a>
```

There are several links that leads to different pages within the Veepee's website and app.
The `href` of each link will differ depending on the user's environment (mobile, desktop, app).
`$getLink()` is a helper function that will print out the desired link for the user based on his environment.

**Syntax**

`$getLink(type, {options})`

**Return value**

Returns a string which must be inserted in the `:href` (or `v-bind:href`) attribute.

**Parameter: `type`**

The `type` will let you decide which link you want to retrieve:

type                   | Description
----------- | ---------------------------------------------------------------------------------------------------------------
`marketplaceHome`      | Link to a marketplace (Brandsplace)
`marketplaceCatalog`   | Link to the catalog of a marketplace (Brandsplace)
`marketplaceProduct`   | Link to a product in any marketplace (Brandsplace)
`marketplaceFilter`    | Link to a universe (or filter or category) in marketplace (Brandsplace)
`home`                 | Link to any tab on the Veepee's website (fashion, home, kids, ...).<br />You can use the home's slug or id (`travel` or `64`).<br />The list of available tabs can be found in the <a href="https://git.vptech.eu/veepee/offerdiscovery/products/brandmanagement/base-vuejs/-/blob/master/src/js/conf/linkHome.js" target="_blank">`src/js/conf/linkHome.js`</a> file
`sale`                 | Link to a sale (do not use this for Travel or Leisure)
`saleDestin`           | Link to a sale (only use this for Travel or Leisure)
`universe`             | Link to a universe (or category) within a sale
`product`              | Link to a single product page
`filter`               | Link to a filtered catalog view (special event)
`subscribe`            | Subscribe the user to the sale
`app`                  | Force any link to open inside the Veepee app's webview
`browser`              | Force any link to open inside the default browser instead of the Veepee app's webview
`sponsor`              | Link to a sponsor. Brand name can be provided

**Parameter: `options`**

The `options` must be an object. Its content depends of the `type` parameter:

<p class="table-centered"></p>

type                 | operationId | universeId | productId | filter | operationCode | href | slug/id | brand |
-------------------- | ----------- | ---------- | --------- |--------| ------------- | ---- | ------- | ------
`marketplaceHome`    | (✔)         |            |           |        |               |      |         |
`marketplaceCatalog` | (✔)         |            |           |        |               |      |         |
`marketplaceProduct` | (✔)         |            | ✔         |        |               |      |         |
`marketplaceBranch`  | (✔)         | (✔)        |           |        |               |      |         |
`home`               |             |            |           |        |               |      | ✔       |
`sale`               | (✔)         |            |           |        |               |      |         |
`saleDestin`         | (✔)         |            |           |        |               |      |         |
`universe`           | (✔)         | (✔)        |           |        |               |      |         |
`product`            | (✔)         | (✔)        | ✔         |        |               |      |         |
`filter`             | (✔)         |            |           | ✔      |               |      |         |
`subscribe`          | (✔)         |            |           |        | ✔             |      |         |
`browser`            |             |            |           |        |               | ✔    |         |
`app`                |             |            |           |        |               | ✔    |         |
`sponsor`            |             |            |           |        |               |      |         | ✔

<aside class="notice">The operationId is optionnal and is only needed if you want to override the default value located in the <a href="https://git.vptech.eu/veepee/offerdiscovery/products/brandmanagement/base-vuejs/-/blob/master/src/js/conf/common.js" target="_blank">`src/js/conf/common.js`</a> file (as `operation.id`)</aside>
<aside class="notice">The universeId is optionnal too and is only needed if you want to override the default value located in the <a href="https://git.vptech.eu/veepee/offerdiscovery/products/brandmanagement/base-vuejs/-/blob/master/src/js/conf/common.js" target="_blank">`src/js/conf/common.js`</a> file (as `operation.universeID`)</aside>

### Bitly

Sometimes, you may receive suspicious redirect links such as **ad.doubleclick.net**. When that's the case, you'll need to change it to a bitly link, so it can works, otherwise Ad Blocks will cancel the redirection.

To create a Bitly, you have to go on their website, sign up or sign in, then chose the Free Plan (it only limits to 10 links/month).

Once you're in your dashboard, you just have to click on the "Create new" button, at the top left of the page, then copy/paste your link in the input, and finally click on the "Create" button at the bottom right of the page.

You can find all the links you converted in the "Links" tab, at the left of the page. It should be something as **bit.ly/3VAYmFL**
