Simple usage with "default" collect :

```vue
<Collect />
```

Collect with a specific name :

```vue
<Collect name="newsletter" />
```

Custom sizes where `firstname` and `lastname` have 50% width :

```vue
<Collect :sizes="{
    firstname: 48,
    lastname: 48
}" />
```


Collect with a success :

```Vue
<Collect @success="doSomething">
    <template #success>
        <!-- success screen here-->
        <p>Thank you for your participation</p>
    </template>
</Collect>

```

```js

methods: {
    doSomething() {
        console.log('kikou success')
    }
}
```