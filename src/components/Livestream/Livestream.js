'use strict'

import { createNamespacedHelpers } from 'vuex'

const { mapState, mapGetters, mapActions } = createNamespacedHelpers('livestream')
const { mapState: mxpMapState } = createNamespacedHelpers('mixpanel')

export default {
    name: 'Livestream',
    props: {
        /**
         * The live ID to use.<br />
         * Example : 6347e3a0c895f8a0b2e256ad/6347e3e8c895f8a0b2e256b1
         */
        liveId: {
            type: String,
            required: true
        },
        /**
         * The button will show when the reminderAt date is exceeded, if filled.<br />
         * If reminderAt isn't used, then the button will show once the startsAt date is exceeded.<br />
         * The livestream needs to have a waiting screen for that.<br />
         * Example : October 09, 2023 18:45:00<br />
         */
        reminderAt: {
            type: String
        },
        /**
         * Live label button before the livestream is over.
         */
        beforeLabel: {
            type: String,
            default: 'Voir le live'
        },
        /**
         * Button label use beforeLabel as long as the livestream isn't off, and we didn't exceed the endsAt date.<br />
         * The button will show at the startsAt date if reminderAt property isn't used.<br />
         * Example : October 09, 2023 19:00:00
         */
        startsAt: {
            type: String,
            required: true
        },
        /**
         * Live label button once the livestream has started.
         */
        liveLabel: {
            type: String,
            default: 'Voir le live'
        },
        /**
         * Button label use afterLabel if the livestream is off and we exceed the endsAt date.<br />
         * Example : October 09, 2023 19:30:00
         */
        endsAt: {
            type: String
        },
        /**
         * Replay label button once the livestream is over.
         */
        afterLabel: {
            type: String,
            default: 'Voir le replay'
        },
        /**
         * Remove the default button used in the component.
         */
        noButton: {
            type: Boolean
        }
    },
    data() {
        return {
            showModal: false,
            miniModal: false,
            backgroundImg: null,
            liveDomain: ''
        }
    },
    computed: {
        ...mapState([
            'btnLabel'
        ]),
        ...mxpMapState([
            'memberID',
            'mixpanelID'
        ]),
        ...mapGetters([
            'getLiveReminder'
        ]),
        manageScroll() {
            return this.showModal && !this.miniModal
        }
    },
    watch: {
        manageScroll(value) {
            if (value) {
                this.$disableScroll() // somehow it doesn't work.
            } else {
                this.$enableScroll()
            }
        },
        memberID() {
            this.setIframeSRC()
        }
    },
    methods: {
        ...mapActions([
            'setLiveData',
            'setStatus',
            'toggleShop'
        ]),
        setIframeSRC() {
            const environment = this.$config.detectEnv.isDesktop ? 'desktop' : 'mobile'
            const lang = this.$config.lang
            this.$refs.liveIframe.setAttribute('src', `${this.liveDomain}/?mid=${this.memberID}&mixid=${this.mixpanelID}&environment=${environment}&site=${lang.site}&lang=${lang.locale}`)
        },
        openModal() {
            this.sendPM({ type: 'OPEN_IFRAME' })
            this.showModal = true
        },
        closeModal() {
            this.sendPM({ type: 'CLOSE_IFRAME' })
            this.showModal = false
            this.setMini(false)
        },
        setMini(value) {
            this.miniModal = value
        },
        sendPM(pmObj) {
            document.querySelector('.livestream__iframe--live').contentWindow.postMessage(pmObj, this.liveDomain)
        }
    },
    mounted() {
        this.setLiveData({
            'beforeLabel': this.beforeLabel,
            'liveLabel': this.liveLabel,
            'afterLabel': this.afterLabel,
            'startsAt': this.startsAt,
            'endsAt': this.endsAt,
            'reminderAt': this.reminderAt
        })
        this.liveDomain = `https://livestream.apps.veepee.com/${this.liveId}`

        // Wait IDs to set iframe SRC
        this.setIframeSRC()

        // Open Modal on LivestreamButton clicked
        this.$root.$on('openLivestreamModal', () => {
            this.openModal()
        })

        let vm = this
        let isRedirecting = false

        // Events received by the iframe
        window.addEventListener('message', function(message) {
            // console.log(message)
            if (message.origin.indexOf('livestream') !== -1) {
                const data = message.data

                // If the user click on the sale btn inside the iframe
                if (data.type === 'live-redirection' && !isRedirecting) {
                    isRedirecting = true
                    const redirectionPopup = document.querySelector('.redirection-popup')

                    // Create a random timer
                    const random = parseInt(Math.random() * 2000)
                    const timeoutInterval = 2000 + random

                    // Show the redirection popup
                    redirectionPopup.classList.add('active')
                    const redirectionTypeId = 3 // 1 = partner, 2 = brandsplace, 3 = flashsales, 4 = home | TODO use redirectionTypeID once Dumbo send it well, instead of 3
                    vm.$tracking.click('Live go to Sale', true, redirectionTypeId)

                    // Redirects the user after the timer ends
                    let timeout = setTimeout(() => {
                        clearTimeout(timeout)
                        const link = vm.$store.state.dumbo.links[data.idBtn]
                        window.top.location.href = vm.$getLink(link.config.type, link.config.options)

                        setTimeout(() => {
                            redirectionPopup.classList.remove('active')
                            isRedirecting = false
                        }, 500)
                    }, timeoutInterval)
                }

                // If the user hide or close the iframe
                if (data.type === 'hide-close-iframe-cta') {
                    vm.toggleShop(data.shouldHideCloseIframeCta)
                }

                // When the status of the livestream change
                if (data.type === 'stream-status') {
                    vm.setStatus(data.isStreamOn)
                }
            }
        })
    }
}
