# Prerequisites
  - Terminal
  - Git
  - Node.js
  - npm
  
# Installation

## Automatic installation

> Download the initialisation script (run this from your work directory, only once)

```shell
# Clone the script's repository
git clone git@git.vptech.eu:veepee/offerdiscovery/products/brandmanagement/initbasesimple.git
 
# Create a symbolic link
ln -s ./initbasesimple/initbasesimple.sh ./init
```

> You can now initalize any project using the script and following the instructions:

```shell
./init
```

This script will clone your working environment, set up the CI, install dependencies and push your new project to gitlab (optionnal)

The source code of this script is available here: [InitBaseSimple](https://git.vptech.eu/veepee/offerdiscovery/products/brandmanagement/initbasesimple)

## Automatic installation for spain

> Run the init script and select "Spain (Privalia)"

```text
./init

? Project type : Spain (Privalia)
? Operation code : PB_XXXXXX_VP
? Options :
? Continue ? Yes
```

> At some point, you will be prompted to add the "dist" folder in your newly created project folder

```text
Please add a "dist" folder at the root of your project's directory, then press enter...
```

> If everything worked, you will see the following message

```text
Please run the Deploy job for the latest pipeline here:
https://git.vptech.eu/veepee/offerdiscovery/products/brandmanagement/PB_XXXXXX_VP/-/pipelines

Then, the mini site will be available at this URL:
https://www.veepee.es/experiences/pb-xxxxxx-vp/
```

Privalia have their own Media sector and own team to create mini sites for their spanish clients.
However, they need to use our architecture to deploy mini sites.

They usually send us all the HTML, CSS and JS files in a archive via Wetransfer.

1. Download the archive from Wetransfer,
2. Run the `./init` script and select `Spain (Privalia)` from the option list,
3. Enter the Operation Code given by the Media Product Manager (usually *PB_XXXXXX_VP*),
4. When asked, extract the archive and put its content in your project folder under a folder named "dist",
5. Go back to your terminal and press Enter,
6. Run the **Deploy** pipeline on your newly created git repository,
7. Your mini site will shortly be available on production at https://www.veepee.es/experiences/pb-xxxxxx-vp/

## Manual installation

> Manual installation from the terminal. Replace "OP_CODE" with your Operation Code

```shell
# Vue.js
git clone git@git.vptech.eu:veepee/offerdiscovery/products/brandmanagement/base-vuejs.git OP_CODE
cd OP_CODE/
git remote rename origin basesimple
git remote add origin git@git.vptech.eu:veepee/offerdiscovery/products/brandmanagement/OP_CODE.git
./script/replace_opcode.sh OP_CODE

# BaseSimple (obsolete)
git clone git@git.vptech.eu:veepee/offerdiscovery/products/brandmanagement/BaseSimple3.git OP_CODE
cd OP_CODE/
git remote rename origin basesimple
git remote add origin git@git.vptech.eu:veepee/offerdiscovery/products/brandmanagement/OP_CODE.git
./script/replace_opcode.sh OP_CODE
```

Clone the and change the destination (replace "OP_CODE" with your Operation Code):

** If you want to use Vue.js: **

`git clone git@git.vptech.eu:veepee/offerdiscovery/products/brandmanagement/base-vuejs.git OP_CODE`

** If you want to use BaseSimple3 (obsolete): **

`git clone git@git.vptech.eu:veepee/offerdiscovery/products/brandmanagement/BaseSimple3.git OP_CODE`

Navigate to your newly created folder:

`cd OP_CODE/`

Rename the origin remote:

`git remote rename origin basesimple`

Add a new origin remote:

`git remote add origin git@git.vptech.eu:veepee/offerdiscovery/products/brandmanagement/OP_CODE.git`

Run the configuration script to enable the CI for your project:

`./script/replace_opcode.sh OP_CODE`
