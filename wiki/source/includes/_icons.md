# Icons

> Some examples...

```html
<div class="icon-arrow_down"></div>
<div class="icon-vp-logo"></div>
<div class="icon-check"></div>
<div class="icon-back"></div>
<div class="icon-burger_menu"></div>
```

> Icons can be used inside ::before and ::after pseudo elements

```html
<span class="my-facebook-icon"></span>
```

```css
.my-facebook-icon::before {
  content: $icon-facebook;
  font-family: icomoon;
}
```

Icons are managed with <a href="https://icomoon.io/app/#/projects" target="_blank">Icomoon</a> and are called with various names prepended with `icon-`.

The icons files are located in the `src/assets/icons/` folder.

**Available icons**

<a href="http://preprod.veepee.fr/experiences/base-vuejs/wiki/icons/demo.html" target="_blank">List of available icons</a>

You can also view all available icons by opening the `src/assets/icons/demo.html` file in your browser

**Add/edit icons**

Icons can be added or edited by using the <a href="https://icomoon.io/app/#/projects" target="_blank">Icomoon icon font Generator</a> :
- click "Import Project" and import the `src/assets/icons/selection.json` file,
- then, click "Load",
- add icons from the library or manually import SVG files,
- click "Generate Font",
- at this point you might need to check if your new icons displays correctly on your browser,
- **important**: once everything is ok, click the settings icon at the bottom right corner and check "Generate Sass variables",
- close the settings modal and hit the "Download" button,
- unzip the whole content of the downloaded file in the `src/assets/icons/` folder,
- **important**: rename the `style.scss` file into `_style.scss` (prefix with an underscore),

Thats it ! You can use your new icons on your website.
