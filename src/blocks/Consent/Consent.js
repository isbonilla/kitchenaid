'use strict'

import { createNamespacedHelpers } from 'vuex'

const { mapGetters, mapActions } = createNamespacedHelpers('loader')

/**
 * Full height introduction screen.<br />
 * Global styling and background images can be changed in the `Home.scss` file.<br />
 */
export default {
    name: 'Consent',
    computed: {
        ...mapGetters([
            'isLoaded',
            'isConsent'
        ])
    },
    beforeMount() {
        this.setConsent(false)
    },
    watch: {
        isConsent(value) {
            if (value) {
                this.$enableScroll()
            }
        }
    },
    methods: {
        ...mapActions([
            'setConsent'
        ]),
        closeConsent() {
            this.$gsap.to('.consent', {
                autoAlpha: 0,
                onComplete: () => {
                    this.setConsent(true)
                }
            })
        }
    }
}
