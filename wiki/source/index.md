---
title: Documentation BaseSimple

toc_footers:
  - <a href="http://preprod.veepee.fr/experiences/base-vuejs/doc/" target="_blank">Components documentation</a>
  - <p></p>
  - <a href="https://git.vptech.eu/veepee/offerdiscovery/products/brandmanagement" target="_blank">GitLab Media Design Team</a>
  - <a href="https://git.vpgrp.io/groups/VP-BrandFront/" target="_blank">GitLab VP-BrandFront (old)</a>
  - <p></p>
  - <a href="https://confluence.vptech.eu/display/MDT/MEDIA+DESIGN+TEAM" target="_blank">Confluence Media Design Team</a>
  - <a href="https://jira.vptech.eu/projects/MDTF" target="_blank">Jira Media Design Team</a>

includes:
  - documentation
  - installation
  - registry
  - architecture
  - npmScripts
  - loading
  - hashMemberId
  - imagemin
  - spacing
  - fonts
  - icons
  - links
  - i18n
  - urlGenerator
  - portal
  - templates
  - collect
  - directives
  - tips
  - deeplinks
  - oredis

search: true
---
