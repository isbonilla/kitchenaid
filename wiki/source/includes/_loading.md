# Loading

```html
<!-- classic img HTML tag -->
<img v-loader class="mob-only" src="https://picsum.photos/400/300?random=1" alt="">

<!-- The <Img> Component do not need the directive -->
<Img src="https://picsum.photos/400/300?random=1" alt="" />

<!-- These images will only affect the loading for mobile devices -->
<Img class="mob-only" src="https://picsum.photos/400/300?random=1" alt="" />
<Img class="mob-only" src="https://picsum.photos/1000/1000?random=1" alt="" />
<img v-loader class="mob-only" src="https://picsum.photos/4000/3000?random=1" alt="" />
```

The &lt;Loader /&gt; component will show up until every image or video element with the `v-loader` directive attached to is it fully loaded.

<aside class="notice">Please note that you do not need to attach the `v-loader` directive for the &lt;Img /&gt; and &lt;Video /&gt; components.</aside>

<aside class="notice">The `v-loader` directive will optimize the loading process when the element has the `.mob-only`, `desktop-only` or `no-desktop` class.</aside>

<aside class="warning">The `v-loader` directive is not implemented for the CSS background-image property yet.</aside>
