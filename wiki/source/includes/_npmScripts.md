# npm scripts

script | Description | Command
------ | ----------- | -------
`dev` | Runs the mini site on a local server (dumbopp).<br />Uses Preproduction DUMBO endpoints. | `export NODE_ENV=dumbopp && vue-cli-service serve`
`serve` | Runs the mini site on a local server (development) | `vue-cli-service serve`
`serve:prod` | Runs the mini site on a local server (production) | `export NODE_ENV=production && vue-cli-service serve`
`build:preprod` | Builds static files for mini site deployment (preproduction) | `export NODE_ENV=preproduction && vue-cli-service build`
`build:prod` | Builds static files for mini site deployment (production) | `export NODE_ENV=production && vue-cli-service build`
`styleguide` | Runs the component documentation on a local server | `vue-cli-service styleguidist`
`styleguide:build` | Builds static files for component documentation deployment | `vue-cli-service styleguidist:build`
`wiki` | Runs the wiki on a local server (development) | `npm install --prefix ./wiki && npm start --prefix ./wiki`
`wiki:build` | Builds static files for the wiki deployment | `npm install --prefix ./wiki && npm run generate --prefix ./wiki`
`build:docs` | Builds static files for the wiki AND the component documentation deployment | `npm install --prefix ./wiki && npm run generate --prefix ./wiki && vue-cli-service styleguidist:build`
