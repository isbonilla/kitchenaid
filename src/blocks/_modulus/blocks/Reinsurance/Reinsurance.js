'use strict'

export default {
    name: 'Reinsurance',
    props: {
        /**
         * The variants of Reinsurance.
         * @values iconList, horizontalList, iconGrid
         */
        variant: {
            type: String,
            default: 'iconList',
            validator: function(value) {
                return ['iconList', 'horizontalList', 'iconGrid'].indexOf(value) !== -1
            }
        },

        /**
         * See <a href="#fonts">Fonts</a> component for supported values.
         */
        title: {
            type: [ String, Object, Array ]
        },

        /**
         * List of JSON objects<br />
         * Available settings for each item: <br />
         * <ul>
         *   <li>`appear` (boolean) : Animate the apparition.</li>
         *   <li>`img` (string) : Image used.</li>
         *   <li> `title` (string) : Text displayed. See <a href="#fonts">Fonts</a> component for supported values.</li>
         * </ul>
         */
        list: {
            type: Array
        },

        /**
         * Custom class to add
         */
        customClass: {
            type: String
        }
    }
}
