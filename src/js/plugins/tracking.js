'use strict'

import common from '../conf/common'
import getMemberID from '../conf/memberID'
import store from '../../store'
import Vue from 'vue'

export default function() {
    getMemberID.then(member => {
        // init Mixpanel
        store.dispatch('mixpanel/initMixpanel', member)

        // Load adotmob libraries
        // eslint-disable-next-line
        var adotmob=function(){function n(n,t,r,e){return"https://dmp.adotmob.com/vp/op?oid="+t+"&t="+e+"&u="+n+"&e="+r}function t(t,r,e,o){(new Image).src=n(t,r,e,o)}var r,e=0;return{init:function(n,o,i){t(n,o,i,0),r=setInterval(function(){t(n,o,i,e+=5),300===e&&clearInterval(r)},5e3)},triggerEvent:function(n,r,e){return t(n,r,e,0)}}}();
        // eslint-disable-next-line
        var _adotmob_=function(n){function t(n,t){var e=function(n,t){return"https://tracker.data-vp.com/track/"+n+"?b="+r+"&p_time="+t}(n,t);e&&((new Image).src=e)}var e,r=function(t){var e=n.location.href,r=new RegExp("[?&]"+t+"=([^&#]*)","i").exec(e);return r?r[1]:null}("vpas"),a=0;return r?(t("visit",0),e=setInterval(function(){t("time",a+=5),150===a&&clearInterval(e)},5e3),{sendEvent:function(n){t(n,0)}}):(console.warn("[ADOTMOB] Could not find vpas qs value"),{sendEvent:function(){}})}(window);

        const dumbo = Vue.prototype.$config.common.dumbo

        // Send view hit to Mixpanel (when loader closes so we can compute the loading time)
        let viewInterval = setInterval(() => {
            if (store.state.loader.contentInitialized && store.getters['loader/loadingData'].loadingTime > 0) {
                // Initialize Adotmob
                adotmob.init((member.consent) ? member.id : `${dumbo.operationCode}_member`, dumbo.operationCode, `${dumbo.operationCode}_tracking`)

                // Send View B2B mini website tracking
                Vue.prototype.$tracking.view()

                // Send stored errors to Mixpanel
                const mixpanelErrors = store.state.loader.mixpanelErrors
                mixpanelErrors.forEach(error => {
                    console.log('send error:', error.label)
                    Vue.prototype.$tracking.error(error.label, error.message)
                })

                clearInterval(viewInterval)
            }
        }, 100)

        // Add $tracking object to Vue prototype
        Vue.prototype.$tracking = {
            /**
             * Event sent when the user loads the mini website
             */
            view: () => {
                // Send hit for Mixpanel
                store.getters['mixpanel/getMixpanel'].track('View B2B mini website', {
                    'Brand Name': dumbo.brandName,
                    'Operation Code': dumbo.operationCode,
                    'Type of website': dumbo.operationType,
                    'Landing Page': store.state.general.previousPageName, // Name of the page the user arrived
                    'Loading Time': store.getters['loader/loadingData'].loadingTime,
                    'API Loading Time': store.getters['loader/loadingData'].apiLoadingTime,
                    'Images Count': store.getters['loader/loadingData'].imgCount,
                    'Videos Count': store.getters['loader/loadingData'].videosCount
                })
            },
            /**
             * Event sent when the user loads a tab
             */
            viewTab: () => {
                // Send hit for Mixpanel
                store.getters['mixpanel/getMixpanel'].track('View tab', {
                    'Brand Name': dumbo.brandName,
                    'Operation Code': dumbo.operationCode,
                    'Previous Page': store.state.general.previousPageName,
                    'Page Name': store.state.general.currentPageName
                })
            },
            /**
             * Event sent when the user clicks on the mini website (except specific clicks as tab and form)
             * @param clickName
             * @param redirectUser
             * @param redirectionTypeId [optional]
             */
            click: (clickName, redirectUser, redirectionTypeId) => {
                if (redirectUser === undefined) console.error('Tracking : Missing redirectUser param in click call')
                // Send click hit for Mixpanel
                store.getters['mixpanel/getMixpanel'].track('Click', {
                    'Page Name': store.state.general.currentPageName,
                    'Brand Name': dumbo.brandName,
                    'Operation Code': dumbo.operationCode,
                    'Click Name': clickName,
                    'Redirection': redirectUser, // Is the user redirected outside ? True / False
                    'Type of Redirection': common.redirectionTypes[redirectionTypeId] // Brandsplace, Flashsales, Website of the brand, Home
                })

                // Send click hit to Adotmob
                adotmob.triggerEvent((member.consent) ? member.id : `${dumbo.operationCode}_member`, dumbo.operationCode, `Mini site ${clickName}`)
            },
            /**
             * Event sent when the user gives his email or phone number
             * @param inputType
             * @param optinType
             */
            form: (inputType, optinType) => {
                // Send click hit for Mixpanel
                store.getters['mixpanel/getMixpanel'].track('Email or phone optin', {
                    'Page Name': store.state.general.currentPageName,
                    'Brand Name': dumbo.brandName,
                    'Operation Code': dumbo.operationCode,
                    'Type of Input': inputType, // Email or Phone Number
                    'Type of Optin': optinType // Newsletter, Get more information, Call me back
                })

                // Send click hit to Adotmob
                adotmob.triggerEvent((member.consent) ? member.id : `${dumbo.operationCode}_member`, dumbo.operationCode, `Mini site Page ${store.state.general.previousPageName} - ${inputType} ${optinType}`)
            },
            /**
             * Event sent when the user loads a livestream (click event)
             * @param showName
             * @param showStatus
             * @param error [optional]
             */
            live: (showName, showStatus, error = false) => {
                // Send click hit for Mixpanel
                store.getters['mixpanel/getMixpanel'].track('Watch livestream', {
                    'Page Name': store.state.general.currentPageName,
                    'Brand Name': dumbo.brandName,
                    'Operation Code': dumbo.operationCode,
                    'Livestream Name': showName,
                    'On Time': showStatus, // Before / On Time / After
                    'Loading Error': error
                })

                // Send click hit to Adotmob
                adotmob.triggerEvent((member.consent) ? member.id : `${dumbo.operationCode}_member`, dumbo.operationCode, `Mini site ${showName}`)
            },
            /**
             * Event sent when the user sends a request to find a store nearby
             * @param city
             */
            storeLocator: (city) => {
                // Send click hit for Mixpanel
                store.getters['mixpanel/getMixpanel'].track('Find a store', {
                    'Page Name': store.state.general.previousPageName,
                    'Brand Name': dumbo.brandName,
                    'Operation Code': dumbo.operationCode,
                    'City': city // Postal Code or City Name
                })

                // Send click hit to Adotmob
                adotmob.triggerEvent((member.consent) ? member.id : `${dumbo.operationCode}_member`, dumbo.operationCode, `Mini site store locator ${store.state.general.previousPageName}`)
            },
            /**
             * Event sent when the user create a brand alert
             */
            brandAlert: () => {
                // Send click hit for Mixpanel
                store.getters['mixpanel/getMixpanel'].track('Create brand alert', {
                    'Page Name': store.state.general.previousPageName,
                    'Brand Name': dumbo.brandName,
                    'Operation Code': dumbo.operationCode
                })

                // Send click hit to Adotmob
                adotmob.triggerEvent((member.consent) ? member.id : `${dumbo.operationCode}_member`, dumbo.operationCode, `Mini site brand alert ${store.state.general.previousPageName}`)
            },
            /**
             * Event sent when there is an error somewhere
             * @param type
             * @param description
             * @param route
             */
            error: (type, description, route = '') => {
                // Send error hit for Mixpanel
                store.getters['mixpanel/getMixpanel'].track('Mini site error', {
                    'Brand Name': common.operation.brandName,
                    'Operation Code': common.dumbo.operationCode,
                    'Error Type': type,
                    'Error Name': description,
                    'Error Route': route
                })

                // Send click hit to Adotmob
                adotmob.triggerEvent((member.consent) ? member.id : `${common.dumbo.operationCode}_member`, common.dumbo.operationCode, `Mini site error ${common.operation.brandName}`)
            }
        }
    })
}
