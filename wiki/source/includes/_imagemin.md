# Image optimization

> Edit the quality settings in the vue.config.js file

```js
new ImageminPlugin({
    disable: process.env.NODE_ENV === 'development',
    plugins: [
        imageminJpegRecompress({
            loops: 4,
            min: 80,
            max: 85,
            quality: 'high'
        }),
        imageminPngquant({
            quality: [0.5, 0.85]
        })
    ]
})
```

JPG and PNG images inside the `@/assets/img` folder gets compressed and optimized on production environment.

You can edit the quality settings in the <a href="https://git.vptech.eu/veepee/offerdiscovery/products/brandmanagement/base-vuejs/-/blob/master/vue.config.js" target="_blank">`vue.config.js`</a> file.

Image compression can be tested locally by running the `npm run serve:prod` command.
