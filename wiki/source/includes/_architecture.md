# Architecture

Folder | Usage
------------- | --------------------------------------------------------------------------------------------------------
`assets/`     | fonts, icons, CSS files, images and videos are stored here
`blocks/`     | Vue "block" components that are meant to be used as "containers" as they have no determined content
`components/` | Other Vue components. the `_mods` folder inside contains components used for "Modular pages" (templates)
`js/`         | Configuration files, Vue directives, helper functions and custom plugins
`store/`      | Vuex store files and modules
