Dropdown one target :

 ```vue
 <Dropdown
    arrow="icons/icon-circle-arrow.svg"
    :title="[{ type: 'h3', text: 'Fat Title' }, { type: 'smallBody', text: 'Short description of the block (2 lines)' }]"
    :items="[
        {
            title: { type: 'bigBody', text: 'Short title 1', fontClass: 'bold' },
            description: {
                type: 'smallBody',
                text: `Pizza ipsum dolor meat lovers buffalo. Melted ranch tossed Philly extra.
                Sausage pesto peppers NY onions NY pepperoni broccoli. Platter extra ham lasagna
                sausage broccoli ranch. Black black steak mushrooms Chicago. Mayo sauce green parmesan
                wing melted buffalo. Marinara banana personal Hawaiian pork party tomato stuffed platter.
                Personal Chicago tomatoes personal lasagna onions pan. Peppers wing sausage onions hand`
            }
        },
        {
            title: { type: 'bigBody', text: 'Short title 2', fontClass: 'bold' },
            description: {
                type: 'smallBody',
                text: `Pizza ipsum dolor meat lovers buffalo. Melted ranch tossed Philly extra.
                Sausage pesto peppers NY onions NY pepperoni broccoli. Platter extra ham lasagna
                sausage broccoli ranch. Black black steak mushrooms Chicago. Mayo sauce green parmesan
                wing melted buffalo. Marinara banana personal Hawaiian pork party tomato stuffed platter.
                Personal Chicago tomatoes personal lasagna onions pan. Peppers wing sausage onions hand`
            }
        },
        {
            title: { type: 'bigBody', text: 'Short title 3', fontClass: 'bold' },
            description: {
                type: 'smallBody',
                text: `Pizza ipsum dolor meat lovers buffalo. Melted ranch tossed Philly extra.
                Sausage pesto peppers NY onions NY pepperoni broccoli. Platter extra ham lasagna
                sausage broccoli ranch. Black black steak mushrooms Chicago. Mayo sauce green parmesan
                wing melted buffalo. Marinara banana personal Hawaiian pork party tomato stuffed platter.
                Personal Chicago tomatoes personal lasagna onions pan. Peppers wing sausage onions hand`
            }
        }
    ]"
/>
```

Dropdown multi target :

 ```vue
 <Dropdown
    showAll
    arrow="icons/icon-circle-arrow.svg"
    :title="[{ type: 'h3', text: 'Fat Title' }, { type: 'smallBody', text: 'Short description of the block (2 lines)' }]"
    :items="[
        {
            title: { type: 'bigBody', text: 'Short title 1', fontClass: 'bold' },
            description: {
                type: 'smallBody',
                text: `Pizza ipsum dolor meat lovers buffalo. Melted ranch tossed Philly extra.
                Sausage pesto peppers NY onions NY pepperoni broccoli. Platter extra ham lasagna
                sausage broccoli ranch. Black black steak mushrooms Chicago. Mayo sauce green parmesan
                wing melted buffalo. Marinara banana personal Hawaiian pork party tomato stuffed platter.
                Personal Chicago tomatoes personal lasagna onions pan. Peppers wing sausage onions hand`
            }
        },
        {
            title: { type: 'bigBody', text: 'Short title 2', fontClass: 'bold' },
            description: {
                type: 'smallBody',
                text: `Pizza ipsum dolor meat lovers buffalo. Melted ranch tossed Philly extra.
                Sausage pesto peppers NY onions NY pepperoni broccoli. Platter extra ham lasagna
                sausage broccoli ranch. Black black steak mushrooms Chicago. Mayo sauce green parmesan
                wing melted buffalo. Marinara banana personal Hawaiian pork party tomato stuffed platter.
                Personal Chicago tomatoes personal lasagna onions pan. Peppers wing sausage onions hand`
            }
        },
        {
            title: { type: 'bigBody', text: 'Short title 3', fontClass: 'bold' },
            description: {
                type: 'smallBody',
                text: `Pizza ipsum dolor meat lovers buffalo. Melted ranch tossed Philly extra.
                Sausage pesto peppers NY onions NY pepperoni broccoli. Platter extra ham lasagna
                sausage broccoli ranch. Black black steak mushrooms Chicago. Mayo sauce green parmesan
                wing melted buffalo. Marinara banana personal Hawaiian pork party tomato stuffed platter.
                Personal Chicago tomatoes personal lasagna onions pan. Peppers wing sausage onions hand`
            }
        }
    ]"
/>
```
