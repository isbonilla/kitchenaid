export const de = {
    consent: {
        txt: [
            'Durch den Zugriff auf diese Inhalte erklärst Du,',
            '<b> dass Du mindestens 18 Jahre alt bist.</b>'
        ],
        yes: 'Ich bin über 18 Jahre alt',
        no: 'Ich bin unter 18 Jahre alt '
    },
    header: {
        title: 'Zeit für neue<br class="no-desktop"/> kulinarische Abenteuer',
        textScroll: 'Entdecke das exklusive Angebot'
    },
    copycode: {
        copytitle: 'mit dem Code',
        copyLabel: 'Zum Kopieren des Codes klicken',
        copiedLabel: 'Zum Kopieren des Codes klicken'
    },
    stacking: {
        title: 'Entdecke das kabellose<br class="no-desktop"> KitchenAid Go System',
        subtitle: 'Volle Leistung ganz ohne Kabel! Nutze Deine Geräte flexibel und überall dort, wo Du sie brauchst.<br>Mit nur einem abnehmbaren Akku für alle Geräte.'
    },
    slider: {
        title: 'Hole das Beste aus Deiner Küchenmaschine heraus',
        subtitle: 'Starte in die warme Jahreszeit mit unseren sommerlichen Farben und passendem Zubehör',
        card1: ''
    },
    categories: {
        title: 'Entdecke unsere Küchenkleingeräte',
        subtitle: 'Kleine Helfer, die Dein Leben leichter machen',
        card1: 'Standmixer',
        card2: 'Wasserkocher',
        card3: 'Toaster',
        card4: 'Food Processor'
    },
    sticky: {
        text: 'Dein Rabatt mit dem Code <b>VPKAJUN25</b>'
    },
    mention: {
        txt: '*Mit dem Code VPKAJUN25 erhalten Sie 20% auf 5KSM193 Standmixer und die neue kabellose KitchenAid Go Serie, sowie 25% auf alle anderen Produkte. Dieses Angebot gilt auf der gesamten Website www.kitchenaid.de für Online-Käufe und solange der Vorrat reicht. Der Rabatt gilt für das gesamte Sortiment, mit Ausnahme des Colour of the Year Standmixers, der Sets, der Personalisierung und der neuen Produkte. Dieses Angebot kann nicht mit anderen Angeboten oder Werbeaktionen kombiniert werden. Dieses Angebot ist gültig vom 11. bis zum 23. Juni 2024.'
    },
    footerMentions: 'KitchenAid'
}
