const path = require('path')

module.exports = {
    title: 'base-vuejs docs',
    exampleMode: 'expand',
    usageMode: 'expand',
    styleguideDir: 'dist/doc',
    copyCodeButton: true,
    sections: [
        {
            name: 'Blocks',
            components: 'src/blocks/**/[A-Z]*.vue',
            ignore: 'src/blocks/_modulus/**/[A-Z]*.vue'
        },
        {
            name: 'Components',
            components: 'src/components/**/[A-Z]*.vue',
            ignore: 'src/components/_mods/**/[A-Z]*.vue'
        },
        {
            name: 'Modulus',
            components: 'src/blocks/_modulus/blocks/**/[A-Z]*.vue',
            ignore: ['src/blocks/_modulus/blocks/Header/Header.vue', 'src/blocks/_modulus/blocks/Header/HeaderAnimation/HeaderAnimation.vue'],
            sections: [
                {
                    name: 'Template Header',
                    components: ['src/blocks/_modulus/blocks/Header/**/[A-Z]*.vue']
                }
            ]
        },
        {
            name: 'Mods',
            components: 'src/components/_mods/**/[A-Z]*.vue'
        },
        {
            name: 'Template Modulus',
            components: 'src/blocks/_modulus/templates/**/[A-Z]*.vue'
        },
        {
            name: 'Mixins',
            sections: [
                {
                    name: 'ScrollAnimations',
                    sections: [
                        {
                            content: 'src/js/mixins/scrollAnimations/apparitions/Readme.md'
                        }
                    ]
                }
            ]
        }
    ],
    ignore: [
        'src/blocks/Footer/Footer.vue',
        'src/blocks/RedirectionPopup/RedirectionPopup.vue',
        'src/components/BackToHome/BackToHome.vue',
        'src/components/Debug/Debug.vue',
        'src/components/_mods/TopBar/TopBar.vue'
    ],
    theme: {
        fontFamily: {
            base: ['VP Sans Next', 'Helvetica', 'Arial'],
            monospace: ['Menlo', 'Monaco', 'Courier New', 'monospace']
        }
    },
    styles: {
        Code: {
            code: {
                whiteSpace: 'pre',
                backgroundColor: 'rgba(0,0,0,0.05)',
                padding: '3px',
                borderRadius: '3px',
                display: 'inline-block'
            }
        }
    },
    getComponentPathLine(componentPath) {
        const name = path.basename(componentPath, '.vue')
        const dir = path.dirname(componentPath).replace('src', '@')
        return `import ${name} from '${dir}/${name}.vue'`
    },
    renderRootJsx: path.join(__dirname, 'styleguide/styleguide.root.js')
}
