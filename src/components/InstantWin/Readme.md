Using slots :

```vue
<InstantWin>
    <template #win="{ prize }">
        <div>
            <p>You won</p>
        </div>
    </template>
    <template #lose="{ prize }">
        <div>
            <p>You lost</p>
        </div>
    </template>
</InstantWin>
```

Using events :

```vue
<InstantWin
    @win="handleWin"
    @lose="handleLose"
/>

<!-- Then declare handleWin(prize) and handleLose(prize) functions in your methods -->
```

You can specify the collect you want to attach to the game using the `collectName` property. If not set, the value will be "default" :

```vue
<InstantWin collectName="another_collect" />
```

Use the `prize` property if you need dynamic prize. If not set, the value will be operation code set in the "common.js" file :

```vue
<InstantWin prize="CRB_ZAG4_200" />
```
