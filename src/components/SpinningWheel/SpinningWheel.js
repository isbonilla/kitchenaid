'use strict'

import gsap from 'gsap'
import axios from 'axios'
import config from '@/js/conf/common.js'
import Collect from '@/components/Collect/Collect.vue'

export default {
    name: 'SpinningWheel',
    components: {
        Collect
    },
    props: {
        /**
         * Name of the Dumbo collect to display prior to the wheel
         */
        collectName: {
            type: String,
            default: 'default'
        },

        /**
         * List of segments and their prize. Each element must have a `win` attribute and a `prize` attribute if `win` is set to `true`.
         */
        segments: {
            type: Array
        },

        /**
         * Name of the email to send when the form is submitted. The name must be specified in the mail-api project.<br />
         * For more information, check the <a href="#">wiki</a>.
         */
        sendEmail: {
            type: String,
            default: ''
        },

        /**
         * The input type of the form.<br />
         * All values available :
         * <ul>
         *     <li>`Email` <i>(default value)</i></li>
         *     <li>`Phone Number`</li>
         * </ul>
         */
        inputType: {
            type: String,
            default: 'Email',
            validator(value) {
                return ['Email', 'Phone Number'].indexOf(value) !== -1
            }
        },

        /**
         * The optin type of the form.<br />
         * All values available :
         * <ul>
         *     <li>`Newsletter` <i>(default value)</i></li>
         *     <li>`Get more information`</li>
         *     <li>`Call me back`</li>
         * </ul>
         */
        optinType: {
            type: String,
            default: 'Newsletter',
            validator(value) {
                return ['Newsletter', 'Get more information', 'Call me back'].indexOf(value) !== -1
            }
        },

        /**
         * Set specific sizes for any field in the collect.<br />
         * Each field is a property where the key is the name of the field and the value is the size of the field.<br />
         * The size of each field should be an Integer representing the width as a percentage (example: `50` for 50% width).<br />
         * Default size is 100% if not specified.
         */
        sizes: {
            type: Object
        }
    },
    data: () => ({
        currentStep: 0, // 0 = Collect | 1 = Wheel | 2 = Result
        hasSpun: false,
        wheel: null,
        indicator: null,
        currentRotation: null,
        lastRotation: null,
        tolerance: null,
        showResult: false,
        selectedPrize: null,
        win: null,
        form: {},
        selectedSegment: null
    }),
    computed: {
        segmentsCount() {
            return this.segments.length
        },
        prizes() {
            const wins = this.segments.filter(item => item.win)
            return [...new Set(wins.map(item => item.prize))]
        },
        winSegments() {
            // Initialize winSegments
            const winSegments = {}
            this.prizes.forEach(p => {
                winSegments[p] = []
            })

            this.segments.forEach((s, i) => {
                if (s.win) winSegments[s.prize].push(i)
            })

            return winSegments
        },
        looseSegments() {
            const looseSegments = []
            this.segments.forEach((s, i) => {
                if (s.win === false) {
                    looseSegments.push(i)
                }
            })
            return looseSegments
        }
    },
    mounted() {
        this.init()
    },
    methods: {
        getRandom(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min
        },
        init() {
            this.wheel = this.$refs.wheel
            this.indicator = this.$refs.indicator
        },
        handleCollectSuccess(form) {
            this.form = form
            this.getInstantWinResult()
            this.currentStep = 1
        },
        getInstantWinResult() {
            this.selectedPrize = this.prizes[this.getRandom(0, this.prizes.length - 1)]
            const instantWinUrl = process.env.NODE_ENV === 'production' ? config.api.prod.instantWin : config.api.preprod.instantWin

            // Instant Win here
            axios.post(instantWinUrl, {
                op: this.selectedPrize,
                email: this.form.data.email
            })
                .then(response => {
                    this.win = response.data.toLowerCase() === 'gagné'

                    let selectedSegment
                    if (this.win) {
                        selectedSegment = this.winSegments[this.selectedPrize][this.getRandom(0, this.winSegments[this.selectedPrize].length - 1)]
                    } else {
                        selectedSegment = this.looseSegments[this.getRandom(0, this.looseSegments.length - 1)]
                    }

                    this.selectedSegment = selectedSegment
                })
        },
        play() {
            if (!this.selectedSegment) return
            this.spinTo(this.selectedSegment)
        },
        spinTo(selectedSegment) {
            // Prevent spinning multiple times
            if (this.hasSpun) return

            this.hasSpun = true

            // Get degrees from segment
            const safeZone = 2
            const singleSegmentAngle = 360 / this.segmentsCount
            const halfSegmentAngle = singleSegmentAngle / 2
            const safeSpacing = halfSegmentAngle + this.getRandom(-halfSegmentAngle + safeZone, halfSegmentAngle - safeZone)

            const selectedSegmentAngle = selectedSegment * singleSegmentAngle
            const fullTurns = 5

            const degrees = (360 * fullTurns) + selectedSegmentAngle + safeSpacing

            // Indicator animation
            const indicatorTl = gsap.timeline({ paused: true })
                .to(this.indicator, {
                    rotation: -10,
                    transformOrigin: '65% 36%',
                    ease: 'Power1.easeOut',
                    duration: 0.13
                })
                .to(this.indicator, {
                    rotation: 3,
                    backgroundColor: 'red',
                    transformOrigin: '65% 36%',
                    ease: 'Power1.easeOut',
                    duration: 0.13
                })

            // Wheel animation
            gsap.timeline()
                .set(this.wheel, {
                    rotation: 0
                })
                .to(this.wheel, {
                    rotation: degrees,
                    transformOrigin: '50% 50%',
                    ease: 'Power4.easeOut',
                    duration: 5,
                    onUpdate: () => {
                        // Indicator animation
                        this.currentRotation = Math.round(gsap.getProperty(this.wheel, 'rotation'))
                        this.tolerance = this.currentRotation - this.lastRotation

                        if (Math.round(this.currentRotation) % (360 / this.segmentsCount) <= this.tolerance) {
                            if (indicatorTl.progress() > 0.3 || indicatorTl.progress() === 0) {
                                indicatorTl.play(0)
                            }
                        }
                        this.lastRotation = this.currentRotation
                    },
                    onComplete: () => {
                        this.currentStep = 2
                        if (this.win) {
                            /**
                             * Triggers when the user wins
                             *
                             * @property {string} selectedPrize
                             */
                            this.$emit('win', this.selectedPrize)
                        } else {
                            /**
                             * Triggers when the user wins
                             *
                             * @property {string} selectedPrize
                             */
                            this.$emit('lose', this.selectedPrize)
                        }
                    }
                })
        }
    }
}
