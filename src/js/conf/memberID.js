'use strict'

import { getCookie } from '@/js/tools'
import mpProperties from '../conf/mpProperties'
import env from '@/js/conf/detectEnv'
import common from '@/js/conf/common'
import lang from '@/js/conf/lang'
import store from "@/store";

/**
 * Get member ID from the infoMember cookie
 * If not found, get it from the mp_properties query string
 */
let memberID = getCookie('infoMember')
if (memberID === null) {
    const mixpanelMemberId = mpProperties['Member ID']
    if (!mixpanelMemberId){
        memberID = 'mid=-1'
    } else {
        memberID = `mid=${mpProperties['Member ID']}`
    }
}

/**
 * For development and preproduction environment only
 * Use the member id from the test account check_vente_fr@vente-privee.com
 */
if (process.env.NODE_ENV !== 'production') {
    memberID = 'mid=3717420'
}


/**
 * Check consent
 */
const checkID = () => {
    let cookielaw = getCookie('OptanonConsent').split(',')
    let consent = cookielaw.includes('C0004:1')
    memberID = memberID.split('=')[1]
    return {id: memberID, consent}
}

/**
 * Export the member ID and consent once the banner is loaded and the user accepted or refused to consent
 * This is skipped for the vp-app environment
 */
const processBanner = (resolve) => {
    if (env.isDesktop) {
        let i = 0
        let waitForOneTrust = setInterval(() => {
            if (typeof(OneTrust) !== 'undefined') {
                let bannerClosed = getCookie('OptanonAlertBoxClosed')
                if(bannerClosed){
                    let member = checkID()
                    // init Mixpanel
                    store.dispatch('mixpanel/initMixpanel', member)
                    resolve(member)
                    clearInterval(waitForOneTrust)
                }

                OneTrust.OnConsentChanged((e) => {
                    let member = checkID()
                    // init Mixpanel
                    store.dispatch('mixpanel/initMixpanel', member)
                    resolve(member)
                    clearInterval(waitForOneTrust)
                })

                resolve({ id: -1 })
            } else {
                i++
                if (i === 10) resolve({ id: -1 })
            }
        }, 100)
    } else {
        let member = {id: memberID.split('=')[1], consent: true}
        resolve(member)
    }
}

export default new Promise(function(resolve, reject) {
    /**
     * Load One Trust script file from CDN
     */
    if(typeof(OneTrust) === 'undefined' && env.isDesktop) {
        const moduleScript = document.createElement('script')
        const oneTrustDomain = common.onetrust[lang.site][lang.domain]

        let dataLanguage
        switch (lang.locale) {
            case 'espr':
                dataLanguage = 'es'
                break
            case 'itpr':
                dataLanguage = 'it'
                break
            case 'befr':
                dataLanguage = 'fr'
                break
            case 'benl':
                dataLanguage = 'nl'
                break
            case 'lu':
                dataLanguage = 'fr'
                break
            case 'at':
                dataLanguage = 'de'
                break
            default:
                dataLanguage = lang.locale
        }
        moduleScript.setAttribute('data-language', dataLanguage)
        moduleScript.setAttribute('data-domain-script', process.env.NODE_ENV !== 'production' ? `${oneTrustDomain}-test` : oneTrustDomain)
        moduleScript.setAttribute('data-document-language', 'true')
        moduleScript.setAttribute('src', `https://cdn.cookielaw.org/scripttemplates/otSDKStub.js`)
        moduleScript.onload = processBanner(resolve)
        document.head.appendChild(moduleScript)
    } else {
        processBanner(resolve)
    }
})
