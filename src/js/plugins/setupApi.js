import axios from 'axios'
import lang from '@/js/conf/lang'
import store from '@/store/'
import i18n from '@/js/plugins/i18n'
import Vue from 'vue'

export default (operationCode = '') => {
    // Get API URL based on environment
    const baseURL = process.env.NODE_ENV === 'dumbopp' ? 'https://preprod.veepee.fr' : lang.APIbaseURL

    // Headers for Axios
    const headers = {
        'Content-Type': 'application/json',
        'Accept': 'text/plain'
    }

    // Setup Axios instance
    const api = axios.create({ baseURL, headers })
    Vue.prototype.$api = api

    let loadingTime = new Date().getMilliseconds()
    let apiResponded = false

    // eslint-disable-next-line no-constant-condition
    if (operationCode !== '' && operationCode !== '[opcode]') {
        api.get(`/api/mediasale/v2/${operationCode}/${lang.siteId}`)
            .then(res => {
                const data = res.data.data
                apiResponded = true
                loadingTime = (new Date().getMilliseconds() - loadingTime)
                store.dispatch('loader/setAPILoadingTime', loadingTime)

                // Translations
                if (data.section) {
                    const messages = data.section
                    i18n.setLocaleMessage(lang.locale, messages)
                    console.log(messages)
                }

                // Links
                if (data.link) {
                    store.dispatch('dumbo/setLinks', data.link)
                    console.table(store.state.dumbo.links)
                }

                // media
                if (data.media) {
                    Vue.prototype.$media = data.media
                    console.table(Vue.prototype.$media)
                }

                // style
                if (data.style) {
                    Vue.prototype.$style = data.style
                    console.table(Vue.prototype.$style)
                }

                // Collects
                if (data.collects) {
                    Vue.prototype.$collects = data.collects
                    console.table(Vue.prototype.$collects)
                }

                // Config
                const dumboConfig = { ...data }
                delete dumboConfig.section
                delete dumboConfig.link
                delete dumboConfig.media
                delete dumboConfig.style

                // Add dumbo config to static config
                Vue.prototype.$config.common.dumbo = dumboConfig
                console.log(dumboConfig)

                // Add dumbo config to store
                store.dispatch('dumbo/setConfig', dumboConfig)

                // Content is initialized
                store.dispatch('loader/contentInitialized')
            })
            .catch(error => {
                apiResponded = true
                loadingTime = (new Date().getMilliseconds() - loadingTime)
                store.dispatch('loader/setAPILoadingTime', loadingTime)

                let data = error.response.data ? error.response.data : null
                if (data) {
                    error = `${data.statusCode} : ${data.message}`
                }

                // Throw an error (no need to wait One Trust)
                store.dispatch('loader/addMixpanelError', {
                    label: 'api',
                    message: `Loading Time : ${loadingTime} | Error : ${error}`
                })

                console.error(error)

                // Content is initialized
                store.dispatch('loader/contentInitialized')
            })

        // if loading since 10 seconds, then send mixpanel error event
        setTimeout((_) => {
            if (!apiResponded) {
                // Immediately send mixpanel hit if the tracking is already initialized, else put the error in the store
                if (Vue.prototype.$tracking) {
                    Vue.prototype.$tracking.error('loading api', 'Waiting data from the API since 10 seconds.')
                } else {
                    store.dispatch('loader/addMixpanelError', {
                        label: 'loading api',
                        message: 'Waiting data from the API since 10 seconds.'
                    })
                }
            }
        }, 10000)
    } else {
        store.dispatch('loader/contentInitialized')
        console.log(i18n.messages[lang.locale])
        console.table(store.state.dumbo.links)
        /* TODO later when it will be ready
        console.table(Vue.prototype.$media)
        console.table(Vue.prototype.$style) */
    }
}
