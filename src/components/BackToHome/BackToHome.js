'use strict'
import lang from '@/js/conf/lang'

export default {
    name: 'BackToHome',
    data: () => ({
        showBackToHome: true,
        lang: lang
    }),
    mounted: function() {
        // Hide if the navBar is setted
        if (document.querySelector('.nav-bar') !== null) {
            this.showBackToHome = false
        }
    }
}
