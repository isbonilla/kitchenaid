With a trad like `legalMentions: '* This is a mention with a redirection on {dumbo_link_id}.'` :

```vue
<DynamicTrad slug="legalMentions" />
```

With a specified tag so it becomes a span :

```vue
<DynamicTrad slug="legalMentions" tag="span" />
```
