'use strict'

import axios from 'axios'
import config from '@/js/conf/common.js'
import Collect from '@/components/Collect/Collect.vue'

export default {
    name: 'InstantWin',
    components: {
        Collect
    },
    props: {
        /**
         * Name of the Dumbo collect to display prior to the wheel
         */
        collectName: {
            type: String,
            default: 'default'
        },

        /**
         * Use this if you have several prizes to win. Defaults to the operation code set in common.js file
         */
        prize: {
            type: String,
            default: config.dumbo.operationCode
        },

        /**
         * Name of the email to send when the form is submitted. The name must be specified in the mail-api project.<br />
         * For more information, check the <a href="#">wiki</a>.
         */
        sendEmail: {
            type: String,
            default: ''
        },

        /**
         * The input type of the form.<br />
         * All values available :
         * <ul>
         *     <li>`Email` <i>(default value)</i></li>
         *     <li>`Phone Number`</li>
         * </ul>
         */
        inputType: {
            type: String,
            default: 'Email',
            validator(value) {
                return ['Email', 'Phone Number'].indexOf(value) !== -1
            }
        },

        /**
         * The optin type of the form.<br />
         * All values available :
         * <ul>
         *     <li>`Newsletter` <i>(default value)</i></li>
         *     <li>`Get more information`</li>
         *     <li>`Call me back`</li>
         * </ul>
         */
        optinType: {
            type: String,
            default: 'Newsletter',
            validator(value) {
                return ['Newsletter', 'Get more information', 'Call me back'].indexOf(value) !== -1
            }
        },

        /**
         * Set specific sizes for any field in the collect.<br />
         * Each field is a property where the key is the name of the field and the value is the size of the field.<br />
         * The size of each field should be an Integer representing the width as a percentage (example: `50` for 50% width).<br />
         * Default size is 100% if not specified.
         */
        sizes: {
            type: Object
        }
    },
    data: () => ({
        currentStep: 0, // 0 = Collect | 1 = Result
        win: null,
        form: {}
    }),
    methods: {
        handleCollectSuccess(form) {
            this.form = form
            this.getInstantWinResult()
            this.currentStep = 1
        },
        getInstantWinResult() {
            const instantWinUrl = process.env.NODE_ENV === 'production' ? config.api.prod.instantWin : config.api.preprod.instantWin

            // Instant Win here
            axios.post(instantWinUrl, {
                op: this.prize,
                email: this.form.data.email
            })
                .then(response => {
                    this.win = response.data.toLowerCase() === 'gagné'

                    if (this.win) {
                        /**
                         * Triggers when the user wins
                         *
                         * @property {string} prize
                         */
                        this.$emit('win', this.prize)
                    } else {
                        /**
                         * Triggers when the user loses
                         *
                         * @property {string} prize
                         */
                        this.$emit('lose', this.prize)
                    }

                    this.currentStep = 1
                })
        }
    }
}
