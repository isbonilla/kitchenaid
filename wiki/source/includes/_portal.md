# Portal

The Portal can be used to redirect any user to a website inside his mobile browser or inside the Veepee App (if they have it installed).

*** Behavior ***

If the user is on <b>desktop</b>, he will be automatically redirected to the site corresponding to the given operation code.
If the user is on <b>mobile</b>, he will have the choice to continue browsing on the Veepee App or on his mobile browser.

*** Usage  ***

1. Use the <b>URL Generator</b> to generate the Portal Link, as seen above
2. On your mobile, if not automatically redirected, select the App or the Browser button to get redirected 
