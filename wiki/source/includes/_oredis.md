# Oredis internal server

Sometimes, you'll need to access to the Oredis internal server where you'll find some assets.

The link provided will looks like `\\oredis-vp\partage\Ventes_en_cours\VENTES 2023\2-Apr-Jun\LACOSTE112\Créations\30_Propositions\LANDING PAGE`

On Windows, you only have to put it in the url of your browser.

On Mac, that's a little trickier. You have to open your Finder, then click on "Aller", then "Se connecter au serveur..."
Once this is done, you'll have to add the url in the input, then add **smb:** before and **replace all the "\" by "/"**.
example : `smb://oredis-vp/partage/Ventes_en_cours/VENTES 2023/2-Apr-Jun/LACOSTE112/Créations/30_Propositions/LANDING PAGE`
Once this is done, you click on "Connexion" and there you go !

![embed code](./images/oredis-finder.png)
