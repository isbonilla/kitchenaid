# Collect

- Add the component into your js  `import Collect from '@/components/Collect/Collect.vue'`

> `Vue template file`

```html
<Collect :sizes="{
            firstname: 48,
            lastname: 48,
            cp: 100,
            ville: 100
}"/>

```

- Dumbo => create a "collect" => add fields => placeholder in input are labels so far.

- Default name is `default` but you can have multiple ones. Then, if the name isn't the one by default, add it in the name property `<Collect name="collect-2" />` 

- You can custom fields sizes with `sizes` property. (Use the name used in DUMBO to customize the size)
